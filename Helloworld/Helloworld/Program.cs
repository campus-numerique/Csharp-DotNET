﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helloworld
{
    public class Message
    {
        public Message(string day, int hour)
        {
            this.Day = day;
            this.Hour = hour;
            Console.WriteLine("Un message a été créé");
        }

        public string Day { get; set; }
        public int Hour { get; set; }

        public string GetHelloMessage
        {
            get
            {
                    string message = string.Empty;
                if ((this.Day == "lundi" && this.Hour < 8) || this.Day == "samedi" || this.Day == "dimanche" || (this.Day == "vendredi" && this.Hour >= 18))
                {
                    message = "Bon week end ";
                } else if (this.Hour >= 8 && this.Hour < 13)
                {
                    message = "Bonjour ";

                } else if (this.Hour <=13 && this.Hour < 18)
                {
                    message = "Bon après-midi ";
                }else
                {
                    { message = "Bon après-midi "; }
                }
                return message;
            }
        }
    }

    class Program
    {
        enum Feelings
        {
            content,
            triste,
            monstrueux,
            éclaté
        }
        

        static void Main(string[] args)
        {
            string user = Environment.UserName;
            string[] tableauNom = user.Split('.');
            string prenom = tableauNom[0][0].ToString().ToUpper() + tableauNom[0].Substring(1);
            string nom = tableauNom[1][0].ToString().ToUpper() + tableauNom[1].Substring(1);
            user = prenom + " " + nom;
            DateTime now = DateTime.Now;
            String day = now.ToString("dddd");
            int hour = now.Hour;
            bool enter = false;
            Message message = new Message(day,hour);
            Console.WriteLine(message.GetHelloMessage + " " + user);

            //Feelings feel = Feelings.content;
            //Console.WriteLine("Comment te sens-tu aujourd'hui?");
            //Console.WriteLine("Ca va je me sens " + feel);
            //string saisie = Console.ReadLine();
            while (!enter)
            {
                Console.WriteLine("Appuyer sur la touche 'entrer' pour continuer, ou taper 'exit' pour sortir : ");
                string answer = Console.ReadLine();

                if (answer == "exit")
                {
                    enter = true;
                }
                else
                {
                    enter = false;
                }
                Console.WriteLine(message.GetHelloMessage + " " + user);
            }
        }
    }
}
