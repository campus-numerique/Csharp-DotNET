﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace iteration2
{
    public class MetroMobiliteService
    {
        
        public static List<Lametro> GetStops()
        {
            // Create a request for the URL.
            WebRequest request = WebRequest.Create("http://data.metromobilite.fr/api/linesNear/json?x=5.728332599999931&y=45.1854744&dist=700&details=true");
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            //Console.WriteLine("Status: " + ((HttpWebResponse)response).StatusDescription + "\n");
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            //Liste obtenu grâce à l'api et désérializé en c#
            List<Lametro> stops = new List<Lametro>();
            stops = JsonConvert.DeserializeObject<List<Lametro>>(responseFromServer);
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
            return stops;
        }

        public static List<DetailsLigne> GetLinesDetails()
        {
            // Create a request for the URL.
            WebRequest request = WebRequest.Create("http://data.metromobilite.fr/api/routers/default/index/routes");
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            //Console.WriteLine("Status: " + ((HttpWebResponse)response).StatusDescription + "\n");
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            //Liste obtenu grâce à l'api et désérializé en c#
            List<DetailsLigne> LineDetails = new List<DetailsLigne>();
            LineDetails = JsonConvert.DeserializeObject<List<DetailsLigne>>(responseFromServer);
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
            return LineDetails;
        }

    }
}
