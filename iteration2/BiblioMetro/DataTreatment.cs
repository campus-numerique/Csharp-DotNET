﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iteration2
{
    public class DataTreatment
    {

        public static Dictionary<String, List<String>> NoDoubles()
        {
            //appel de la méthode pour récupérer les infos de l'api
            List<Lametro> stops = MetroMobiliteService.GetStops();
            //On élimine les doubles de la liste et enregistre le tout dans un dictionnaire
            Dictionary<String, List<String>> nodoubles = new Dictionary<String, List<String>>();

            foreach (Lametro stop in stops)
            {
                if ((nodoubles.ContainsKey(stop.name)))
                {
                    stop.lines.ForEach(delegate (string line)
                    {
                        if (!nodoubles[stop.name].Contains(line))
                        {
                            nodoubles[stop.name].Add(line);
                        }
                    });
                }
                else
                {
                    nodoubles.Add(stop.name, stop.lines);
                }
            }
            return nodoubles;
        }

        public static Dictionary<string, List<DetailsLigne>> dicoDetails()
        {
            Dictionary<String, List<String>> nodoubles = DataTreatment.NoDoubles();
            Dictionary<String, List<DetailsLigne>> dicoDetails = new Dictionary<String, List<DetailsLigne>>();
            List<DetailsLigne> lineDetails = MetroMobiliteService.GetLinesDetails();
            DetailsLigne details = null;

            foreach (KeyValuePair<string, List<String>> nodouble in nodoubles)
            {
                List<DetailsLigne> stopDetails = new List<DetailsLigne>();
                foreach (String line in nodouble.Value)
                {
                    details = getDetails(line);
                    stopDetails.Add(details);
                }
                dicoDetails.Add(nodouble.Key, stopDetails);
            }
            return dicoDetails;
        }

        public static DetailsLigne getDetails(string id)
        {
            List<DetailsLigne> lineDetails = MetroMobiliteService.GetLinesDetails();
            DetailsLigne detail = null;
            foreach (DetailsLigne lineDetail in lineDetails)
            {
                if (lineDetail.id.Equals(id))
                {
                    detail = lineDetail;
                }
            }
            return detail;
        }
    }
}
