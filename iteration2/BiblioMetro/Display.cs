﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iteration2
{
    public class Display
    {

        public static void PrintNodoubles( )
        {
            Dictionary<String, List<String>>  nodoubles = DataTreatment.NoDoubles();
            List<DetailsLigne> detailsLigne = MetroMobiliteService.GetLinesDetails();
            Console.Write("######### Sans doublons ############\n");
            // affiche les infos sans doublons de l'api
            foreach (KeyValuePair<string, List<String>> nodouble in nodoubles)
            {
                Console.WriteLine("nom de l'arrêt: " + nodouble.Key);
                nodouble.Value.ForEach(details => Console.WriteLine("Ligne de transport: " + details));
                Console.WriteLine("\n");
            }
        }

        public static void PrintNodoubleDetails()
        {
            Dictionary<String, List<DetailsLigne>> nodoubleDetails = DataTreatment.dicoDetails();
            Console.Write("######### Sans doublons avec les details des lignes ############");
            Console.WriteLine("\n");
            // affiche les infos sans doublons avec détails des lignes de l'api
            foreach (KeyValuePair<string, List<DetailsLigne>> nodoubleDetail in nodoubleDetails)
            {
                Console.WriteLine("Arrêt: " + nodoubleDetail.Key);
                //nodoubleDetail.Value.ForEach(details => Console.WriteLine("details: " + details.longName));
                nodoubleDetail.Value.ForEach(details => PrintDetails(details));
                Console.WriteLine("\n");
            }
        }
        public static void PrintDetails(DetailsLigne detail)
        {
            // affiche les détails des lignes d'un arret
            Console.WriteLine("Ligne: " + detail.longName);
            Console.WriteLine("Type de transport: " + detail.mode);
            
        }
    }
}
