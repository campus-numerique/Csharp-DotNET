﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace iteration2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Pour éviter les erreurs de communications
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //Display.PrintNodoubles();
            Display.PrintNodoubleDetails();
            Console.ReadLine();
        }
    }
}
